<?php namespace Whitegolem\LaraCrud\Routing\Controllers;

use Illuminate\Routing\Controllers\Controller;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

abstract class BaseController extends Controller {

	/**
	 * The name of the model class.
	 *
	 * @var string
	 */
	protected $modelName;

	/**
	 * The model associated to the controller.
	 *
	 * @var string
	 */
	protected $model;

	/**
	 * The key used for JSON results.
	 *
	 * @var string
	 */
	protected $resourceKey;

	/**
	 * The key used for JSON result.
	 *
	 * @var string
	 */
	protected $resourceKeySingular;

	/**
	 * The method used to interact with the nested resources.
	 *
	 * @var string
	 */
	protected $relation;

	/**
	 * The path to the views folder.
	 *
	 * @var string
	 */
	protected $viewPath;

	/**
	 * Indicates if the results should be paginated.
	 *
	 * @var bool
	 */
	protected $paginate = true; //true to enable pagination by default

	public function __construct()
	{

	}

	
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

	/**
   	 * abstarct function. must be implemented to get the path to the views
   	 *
   	 */
	abstract public function getViewPath();

	/**
   	 * abstarct function. must be implemented to get the resource name
   	 *
   	 * @return string the key used to display json results, typically a plural noun
   	 */
	abstract public function getResourceName();

	/**
   	 * Get a segment from the Controller
   	 *
   	 * @param  int 		$index
   	 * @param  string	$default
     * @return string
   	 */
   	protected function controllerSegment($index=0, $default = null)
   	{
		$classBaseName = class_basename($this);
		$controllername = preg_replace('/Controller$/', '', $classBaseName); //remove the string 'Controller' from the class name
		$segments = preg_split('/(?=[A-Z])/', $controllername, -1, PREG_SPLIT_NO_EMPTY); //split on capital letters using positive lookahead (?=)
		$segment = isset($segments[$index]) ? $segments[$index] : $default;

		return $segment;
   	}

	/**
   	 *
   	 * @param  string	$singular Indicates if the key should be in singular form
   	 * @return string the key used to display json results, typically a plural noun
   	 */
	protected function getResourceKey($singular=false)
	{
		$resourceName = $this->getResourceName();
		if($singular)
		{
			if (isset($this->resourceKeySingular)) return $this->resourceKeySingular;
			return Str::singular($resourceName);
		}
		if (isset($this->resourceKey)) return $this->resourceKey;
		return Str::plural($resourceName);
	}

	/**
	 * get the class name of the model
	 */
	protected function getModelName()
	{
		if (isset($this->modelName)) return $this->modelName;

		$controllerName = $this->controllerSegment(0);


		return Str::singular($controllerName);
	}

	/**
	 * set the the model associated to the controller
	 */
	public function setModel($className=null)
	{
		$className = $className ?: $this->getModelName();
		$this->model = new $className();
	}

	/**
	 * get an instance of the model associated to the controller
	 */
	public function getModel()
	{
		if(!$this->model) $this->setModel(); //create a new instance of the model if not present
		return $this->model;
	}

	/**
	 * builds the path of the view starting from a base (usually the controller name IE the plural form of the model)
	 *
	 * @param $array one or more views to concatenate
	 * @return string the path of the view
	 */
	protected function buildViewPath($views=array(), $base=null)
	{
		$base = $base ?: $this->getViewPath();

		if(!is_array($views)) $views = array($views);
		array_unshift($views, $base);
		$path = implode('.', $views);
		return $path;
	}

	/**
	* apply the request params to the query
	*
	* @param mixed $object Model/Relation a model query or a relation to be filtered
	* @return array a modified query and eventually a Paginator
	*/
	private function applyParams($query)
	{
		//search, sort
		$query = $query
					->eagerLoad(Input::get('with'))
					->filterWhere(Input::get('where'))
					->searchAny(Input::get('q'))
					->sortBy(Input::get('sort'));

		/**
		* use this trigger to customize the query
		*
		*	Event::listen('after.defaultParams', function($query)
		*	{
		*		$query->filterWhere(array('denominazione','like','%ponte%'));
		*	});
		*
		*/
		Event::fire('after.defaultParams', array(&$query));

		return $query;
	}

	public $queryCallables = [];

	/**
	 * get an array of data to use in the response
	 * @param Query $query the query to use to retrieve the data
	 * @param Paginator $paginator
	 * @param array $additionalData an associative array of data to merge with the data array
	 */
	protected function getData($query=null, $additionalData = array())
	{
		if(is_null($query)) return;
		$query = $this->applyParams($query);

		//use this hook to alter the parameters
		$paginator = null;

		if( (Input::get('page') !== '0') && (Input::get('page') || $this->paginate) )
		{
			$beforePagination = Event::fire('before.pagination', array(&$query));

			//check if $object is a model or a relation
			$model = $query->getModel();
			if(method_exists($model, 'getRelated'))  $model = $model->getRelated();

			$perPage = Input::get('pp') ?: $model->getPerPage();
			$paginator = $query->paginate($perPage);

			//preserve the url query in the paginator
			$paginator->appends(Input::except('page'));
		}

		$results = isset($paginator) ? $paginator->getCollection() : $query->get();
		$resourceKey = $this->getResourceKey();
		
		$data = array(
			$resourceKey => array(),
			'meta' => array()
		);
		if(isset($paginator))
		{
			$results = $paginator->getCollection();
			$data['meta']['total'] = $paginator->getTotal();
			$data['meta']['paginator'] = $paginator;

		}else
		{
			$results = $query->get();
			$data['meta']['total'] = $results->count();
		}
		$data[$resourceKey] = ($this->isAjaxRequest()) ? $results->toArray() : $results;

		if(is_array($additionalData)) $data = array_merge($data, $additionalData);

		return $data;
	}

	/**
	 * checks wheter it's an ajax request or not
	 * in case of a jsonp request sets $callback to the name of the callback function
	 */
	protected function isAjaxRequest()
	{
		$callback = Input::get('callback', false);

		return (Request::ajax() || $callback);
	}

	/**
	 * prints a json response.
	 * in case of a jsonp request wraps the json data with a callback function
	 *
	 * @param array $data the data to convert to json
	 * @param int $status the status code to return in the response
	 * @return json data
	 */
	protected function jsonResponse($data = array(), $status = 200)
	{
		$response = \Response::json($data,$status);

		if($callback = Input::get('callback')) $response = $response->setCallback($callback);

		return $response;
	}

	/**
	* generates an error response when the model is not found or redirects home with an error
	*
	* @param String $message the message to print
	* @return Response
	*/
	protected function modelNotFoundError($message = 'elemento non trovato')
	{
		$data = array(
			'error' => true,
			'message' =>$message
		);

		if($this->isAjaxRequest())
		{
			return $this->jsonResponse($data,404);
		}
		return Redirect::home()->with('error', $message);
	}

	/**
	* generates an error response when the model cannot be saved
	*
	* @param Model $model the model we tried to save
	* @return Response
	*/
	protected function savingError($model)
	{
		$data = array(
			'error' => true,
			'message' => 'saving error',
			'errors' => $model->errors->toArray()
		);

		if($this->isAjaxRequest())
		{
			return $this->jsonResponse($data,400);
		}
		return Redirect::back()->withInput()->withErrors($model->errors);
	}

	/**
	* generates an error response when the model cannot be saved
	*
	* @param Model $model the model we tried to save
	* @return Response
	*/
	protected function methodNotAllowedError($model)
	{
		$data = array(
			'error' => true,
			'message' => 'method not allowed',
			'errors' => $model->errors->toArray()
		);

		if($this->isAjaxRequest())
		{
			return $this->jsonResponse($data,400);
		}
		return Redirect::back()->withInput()->withErrors($model->errors);
	}


}