<?php namespace Whitegolem\LaraCrud\Routing\Controllers;


use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

abstract class CrudController extends BaseController {

	public function __construct()
	{
		parent::__construct();

	}

	/**
   	 * Get the name of the resource
   	 *
   	 * @return string the key used to display json results, typically a plural noun
   	 */
	public function getResourceName()
	{
		$modelName = $this->getModelName();
		$lowerCaseModelName = Str::lower( $modelName );
		return Str::plural( $lowerCaseModelName );
	}

	public function getViewPath()
	{
		if (isset($this->viewPath)) return $this->viewPath;

		$classNameParts = explode('\\', get_class($this));
		
		array_pop($classNameParts); //leave only the namespace
		array_push($classNameParts,Str::plural($this->getModelName()));
		$basePath = implode('.',$classNameParts);
		return Str::lower($basePath);
	}

	/**
	 * Display a listing of the resource.
	 *
	 *
	 * For example to add default pagination in a controller:
	 * Event::listen('before.query', function(&$parameters){
	 *		if(!in_array('page', $parameters)) $parameters['page'] = 1;
	 *	});
	 *
	 *
	 * @return Response
	 */
	public function index()
	{
		$model = $this->getModel();
		$query = $model->query();

		$data = $this->getData($query);

		if($this->isAjaxRequest())
		{

			return $this->jsonResponse($data,200);
		}
		
		$viewPath = $this->buildViewPath("index");
		$this->layout->content = View::make($viewPath, $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$this->buildViewPath("create");

		$viewPath = $this->buildViewPath("create");
		$this->layout->content = View::make($viewPath);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
		
		$model = $this->getModel()->fill($input);

		//controlla se ci sono problemi durante il salvataggio
		if(! $model->save() ) return $this->savingError($model);

		$message = 'nuovo elemento creato';

		if($this->isAjaxRequest())
		{
			$data = array();
			$data[$this->getResourceKey(true)] = $model->toArray();
			$data['message'] = $message;


			return $this->jsonResponse($data,201);
		}

		$viewPath = $this->buildViewPath("show");
		return Redirect::route($viewPath, array($model->id))->with('success', $message);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{

		$model = $this->getModel()->find($id);

		if(is_null($model)) return $this->modelNotFoundError();

		$data = array(
			$this->getResourceKey(true) => $model
		);

		if($this->isAjaxRequest())
		{
			$data[$this->getResourceKey(true)] = $model->toArray();

			$beforeResponse = Event::fire('before.response', array(&$data));
			
			return $this->jsonResponse($data,200);
		}

		$beforeResponse = Event::fire('before.response', array(&$data));

		$viewPath = $this->buildViewPath("show");
		$this->layout->content = View::make($viewPath, $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		$model = $this->getModel()->find($id);

		if(is_null($model)) return $this->modelNotFoundError();

		$data = array(
			$this->getResourceKey(true) => $model
		);


		$viewPath = $this->buildViewPath("edit");
		$this->layout->content = View::make($viewPath, $data);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$model = $this->getModel()->find($id);

		//se l'elemento non è presente creane uno nuovo
		if(is_null($model)) return $this->store();

		$input = Input::all();
		$model->fill($input);

		//controlla se ci sono problemi durante il salvataggio
		if(! $model->save() ) return $this->savingError($model);

		$message = 'elemento aggiornato';

		if($this->isAjaxRequest())
		{
			$data = array();
			$data[$this->getResourceKey(true)] = $model->toArray();
			$data['message'] = $message;

			
			return $this->jsonResponse($data,200);
		}

		$viewPath = $this->buildViewPath("show");
		return Redirect::route($viewPath, array($id))->with('success', $message);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$model = $this->getModel()->find($id);

		$model->delete();


		$message = 'elemento eliminato';

		if($this->isAjaxRequest())
		{
			$data = array();
			$data[$this->getResourceKey(true)] = $model->toArray();
			$data['message'] = $message;

			
			return $this->jsonResponse($data,200);
		}

		$viewPath = $this->buildViewPath("index");
		return Redirect::route($viewPath)->with('success', $message);
	}


	/**
	 *
	 * ---- RELATIONS ----
	 *
	 */

	private function getRelatedMethod()
	{
		$requestSegments = Request::segments();

		if(count($requestSegments)>=3) return array_pop($requestSegments);

		return null;
	}
	private function getRelatedModel()
	{
		$method = $this->getRelatedMethod();
		$model = $this->getModel();
		$relation = $model->$method();
		return $relation->getRelated();
	}

	/**
	 * Display a listing of the related resource.
	 *
	 * @return Response
	 * @param  int  $id
	 */
	public function relatedIndex($id, $viewPath=null)
	{
		$method = $this->getRelatedMethod();

		$model = $this->getModel()->find($id);

		//set the view to use in the response
		if(!$viewPath)
		{
			$viewPath = $this->buildViewPath("index",$method); 
			if($method == Str::singular($method)) $viewPath = $this->buildViewPath("show",$method);
		}

		$relation = call_user_func(array($model, $method));
		list($query, $paginator) = $this->applyParams($relation);

		// get the related items
		$related = isset($paginator) ? $paginator->getCollection() : $query->get();

		$data = array();
		$data[$method] = $related;
		$data['total'] = ($paginator) ? $paginator->getTotal() : $related->count();
		if($paginator) $data['paginator'] = $paginator;

		if($this->isAjaxRequest())
		{
			$data[$method] = $related->toArray();


			return $this->jsonResponse($data,200);
		}

		$this->layout->content = View::make($viewPath, $data);
	}

	/**
	 * Display the specified related resource.
	 *
	 * @param  int  $id
	 * @param  int  $relatedId
	 * @return Response
	 */
	public function relatedShow($id,$relatedId)
	{}

	/**
	 * set belongsTo
	 */
	public function relatedAssociate($id,$relatedId)
	{
		$method = $this->getRelatedMethod();
		$relatedModel = $this->getRelatedModel();


		if( $related = $relatedModel->find($relatedId) )
		{
			$model = $this->getModel()->find($id);


			$model->$method()->associate($related);

			if(! $model->save() ) return $this->savingError($model);
			
			$message = 'relazione salvata';

			if($this->isAjaxRequest())
			{
				$data = array();
				$data[$method] = $related->toArray();
				$data['message'] = $message;
				
				return $this->jsonResponse($data,200);
			}

			$viewPath = $this->buildViewPath("index");
			return Redirect::route($viewPath)->with('success', $message);
		}
		return $this->modelNotFoundError();
	}

	/**
	 * set belongsToMany
	 */
	public function relatedAttach($id,$relatedId)
	{
		$method = $this->getRelatedMethod();
		$relatedModel = $this->getRelatedModel();

		if( $related = $relatedModel->find($relatedId) )
		{
			$model = $this->getModel()->find($id);
			$pivotData = Input::get('pivot', array());
			if(!is_array($pivotData)) $pivotData = (array)$pivotData;

			$model->$method()->detach($relatedId); //detach first to avoid duplicates
			$model->$method()->attach($relatedId, $pivotData);
			
			$message = 'relazione salvata';

			if($this->isAjaxRequest())
			{
				$data = array();
				$data[$method] = $related->toArray();
				$data['message'] = $message;
				
				return $this->jsonResponse($data,200);
			}

			$viewPath = $this->buildViewPath("index");
			return Redirect::route($viewPath)->with('success', $message);
		}
		return $this->modelNotFoundError();
	}
	
	/**
	 * updates pivot data in belongsToMany
	 */
	public function relatedUpdate($id,$relatedId)
	{}

	/**
	 * unset belongsToMany
	 */
	public function relatedDetach($id,$relatedId)
	{
		$method = $this->getRelatedMethod();

		$model = $this->getModel()->find($id);

		if( $related = $model->$method()->get()->find($relatedId) )
		{
			$model->$method()->detach($relatedId);
			
			$message = 'relazione eliminata';

			if($this->isAjaxRequest())
			{
				$data = array();
				$data[$method] = $related->toArray();
				$data['message'] = $message;
				
				return $this->jsonResponse($data,200);
			}

			$viewPath = $this->buildViewPath("index");
			return Redirect::route($viewPath)->with('success', $message);
		}
		return $this->modelNotFoundError();
	}

}