<?php namespace Whitegolem\LaraCrud\Routing\Controllers;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\Redirect;

abstract class NestedController extends BaseController {
/*
 * GET /artworks/{artworks}/authors					| artworks.authors.index	| ArtworksAuthorsController@index	| show all authors of a specified artwork 
 * GET /artworks/{artworks}/authors/create			| artworks.authors.create	| ArtworksAuthorsController@create	| 
 * POST /artworks/{artworks}/authors				| artworks.authors.store	| ArtworksAuthorsController@store 	| create a relation (eventually with pivot data) between an artwork and an author
 * GET /artworks/{artworks}/authors/{authors}		| artworks.authors.show		| ArtworksAuthorsController@show 	| show data (and pivot data) of the author related to the artwork
 * GET /artworks/{artworks}/authors/{authors}/edit	| artworks.authors.edit		| ArtworksAuthorsController@edit 	|
 * PUT /artworks/{artworks}/authors/{authors}		| artworks.authors.update	| ArtworksAuthorsController@update 	| update data (and pivot data) of of the author related to the artwork 
 * PATCH /artworks/{artworks}/authors/{authors}		| 							| ArtworksAuthorsController@update 	|
 * DELETE /artworks/{artworks}/authors/{authors}	| artworks.authors.destroy	| ArtworksAuthorsController@destroy | delete a link between a specified artwork and a specified author
 */

	protected $nestedModelName;

	public function __construct()
	{

	}

	/**
   	 * Get the name of the resource
   	 *
   	 * @return string the key used to display json results, typically a plural noun
   	 */
	public function getResourceName()
	{
		$relatedName = $this->getRelatedName();
		$lowerCaseRelatedName = Str::lower( $relatedName );
		return Str::plural( $lowerCaseRelatedName );
	}

	public function getViewPath()
	{
		if (isset($this->viewPath)) return $this->viewPath;

		$classNameParts = explode('\\', get_class($this));
		array_pop($classNameParts); //leave only the namespace

		array_push($classNameParts,Str::plural($this->getModelName()));
		array_push($classNameParts,Str::plural($this->getRelatedName()));
		$basePath = implode('.',$classNameParts);
		return Str::lower($basePath);
	}

	/**
	 * get the class name of the related model
	 */
	public function getRelatedName()
	{
		if (isset($this->relatedName)) return $this->relatedName;

		$controllerName = $this->controllerSegment(1);
		return Str::singular($controllerName);
	}

	/**
	 * get the name of the method to use to interact with nested resource
	 */
	public function getRelationName()
	{
		if (isset($this->relation)) return $this->relation;

		return Str::lower( $this->getResourceName() );
	}

	/**
	 * Display a listing of the resources related with
	 * a specified model and it's relation (pivot data).
	 *
	 * @return Response
	 */
	public function index($id)
	{
		$modelName = $this->getModelName();
		$model = $this->getModel($modelName)->find($id);

		$relatedName = $this->getRelatedName();
		$relatedModel = $this->getModel($relatedName);

		$relationName = $this->getRelationName();
		$query = $model->$relationName();

		$data = $this->getData($query);

		if($this->isAjaxRequest())
		{
			//use this hook to alter the data of the view
			$beforeResponse = Event::fire('before.response', array(&$data));

			return $this->jsonResponse($data,200);
		}

		//use this hook to alter the data of the view
		$beforeResponse = Event::fire('before.response', array(&$data));
		
		$viewPath = $this->buildViewPath("index");


		$this->layout->content = View::make($viewPath, $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create($id)
	{
		//
	}

	/**
	 * Store a newly created relation between a specified model and a nested resource.
	 *
	 * @return Response
	 *
	 * per test da browser:
	 *	
	 *	$.ajax({
	 *		url:'/artworks/78/authors',
	 *		type:'POST',
	 *		data:{
	 *			nome_scelto:'Francesco Delacqua',
	 *			nome:'francesco',
	 *			scuola:'ermetici',
	 *			joining:{
	 *				riferimento:'attribuito'
	 *			}
	 *		}
	 *	}).done(function(){
	 *		console.log(arguments);
	 *	});
	 *
	 */
	public function store($id)
	{
		//

		$model = $this->getModel()->find($id);
		$relationName = $this->getRelationName();


		if ( method_exists($model, $relationName) && is_a($model->$relationName(),'Illuminate\Database\Eloquent\Relations\Relation') ) {
			$relation = $model->$relationName();
			$relationClass =  get_class($relation);

			$attributes = Input::all(); //pivot data

			$relatedInstance = $relation->getRelated()->newInstance($attributes);
			if(! $relatedInstance->save() ) return $this->savingError($relatedInstance);

			switch($relationClass)
			{
				case 'Illuminate\Database\Eloquent\Relations\BelongsToMany':

					$joining = array();
					if(array_key_exists('joining', $attributes))
						$joining = $attributes['joining'];
					
					$relation->attach($relatedInstance, $joining);
					break;
				case 'Illuminate\Database\Eloquent\Relations\HasOneOrMany':
				case 'Illuminate\Database\Eloquent\Relations\HasOne':
				case 'Illuminate\Database\Eloquent\Relations\HasMany':
					$relation->save($relatedInstance);
					break;
			}

			$message = 'nuovo elemento creato e associato';

			//response
			if($this->isAjaxRequest())
			{
				$data = array();
				$data[$this->getResourceKey(true)] = $relatedInstance->toArray();
				$data['message'] = $message;

				$beforeResponse = Event::fire('before.response', array(&$data));

				return $this->jsonResponse($data,201);
			}

			$viewPath = $this->buildViewPath("show");
			return Redirect::route($viewPath, array($relatedInstance->id))->with('success', $message);
		}

		return methodNotAllowedError($model);

	}

	/**
	 * Display the specified nested resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id, $nestedId)
	{
		$model = $this->getModel()->find($id);

		$relationName = $this->getRelationName();

		$relation = $model->$relationName();



		$otherKey = $relation->getOtherKey();

		$relatedInstance =$relation->where($otherKey, '=', $nestedId)->first();

		if(is_null($relatedInstance)) return $this->modelNotFoundError();

		$data = array(
			$this->getResourceKey(true) => $relatedInstance
		);

		if($this->isAjaxRequest())
		{
			$data[$this->getResourceKey(true)] = $model->toArray();

			$beforeResponse = Event::fire('before.response', array(&$data));
			
			return $this->jsonResponse($data,200);
		}

		$beforeResponse = Event::fire('before.response', array(&$data));

		$viewPath = $this->buildViewPath("show");
		$this->layout->content = View::make($viewPath, $data);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id, $nestedId)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id, $nestedId)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id, $nestedId)
	{
		//
	}

	

}

/**
 * GET /admin/users/{users}/offices                | admin.users.offices.index         | Admin\UsersOfficesController@index
 * GET /admin/users/{users}/offices/create         | admin.users.offices.create        | Admin\UsersOfficesController@create
 * POST /admin/users/{users}/offices               | admin.users.offices.store         | Admin\UsersOfficesController@store
 * GET /admin/users/{users}/offices/{offices}      | admin.users.offices.show          | Admin\UsersOfficesController@show
 * GET /admin/users/{users}/offices/{offices}/edit | admin.users.offices.edit          | Admin\UsersOfficesController@edit
 * PUT /admin/users/{users}/offices/{offices}      | admin.users.offices.update        | Admin\UsersOfficesController@update
 * PATCH /admin/users/{users}/offices/{offices}    |                                   | Admin\UsersOfficesController@update
 * DELETE /admin/users/{users}/offices/{offices}   | admin.users.offices.destroy       | Admin\UsersOfficesController@destroy
 */